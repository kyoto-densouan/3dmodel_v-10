# README #

1/3スケールのCanon V-10風小物のstlファイルです。 スイッチ類、I/Oポート等は省略しています。

組み合わせたファイルとパーツごとに配置したファイルの二種類があります。 元ファイルはAUTODESK Fusion360です。

***

# 実機情報

## メーカ
- Canon

## 発売時期
- 1984年

## 類似モデル
- V-20

## 参考資料

- [ボクたちが愛した、想い出のパソコン・マイコンたち](https://akiba-pc.watch.impress.co.jp/docs/column/retrohard/1218277.html)

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_v-10/raw/d05eccef3e1af02b86fc3a196b1adf72df256267/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_v-10/raw/0d6ba52971c86ee574243a7c3edd62a6726b0067/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_v-10/raw/d05eccef3e1af02b86fc3a196b1adf72df256267/ExampleImage.png)
